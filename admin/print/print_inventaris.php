<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 9%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Data Inventaris</h1>
<table border="1" width="100%">
<tr>
	<th align="center">No.</th>
	<th align="center">Kode Inventaris </th>
	<th align="center">Nama</th>
	<th align="center">Kondisi</th>
	<th align="center">Keterangan</th>
	<th align="center">jumlah </th>
	<th align="center">Jenis </th>
	<th align="center">Ruang </th>
	<th align="center">Petugas </th>
	<th align="center">Tanggal Masuk </th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn, "SELECT * FROM inventaris s left join ruang p on p.id_ruang=s.id_ruang
																left join jenis q on q.id_jenis=s.id_jenis
																left join petugas r on r.id_petugas=s.id_petugas");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr>
			<td align="center"><?php echo $no++; ?></td>
			<td><?php echo $data['kode_inventaris']; ?></td>
			<td><?php echo $data['nama']; ?></td>
			<td><?php echo $data['kondisi']; ?></td>
			<td><?php echo $data['keterangan']; ?></td>
			<td><?php echo $data['jumlah']; ?></td>
			<td><?php echo $data['nama_jenis']; ?></td>
			<td><?php echo $data['nama_ruang']; ?></td>
			<td><?php echo $data['nama_petugas']; ?></td>
			<td><?php echo $data['tanggal_register']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Petugas.pdf', 'D');
?>