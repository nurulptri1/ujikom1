<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 16%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Data Petugas</h1>
<table border="1" width="100%">
<tr>
	<th align="center">No.</th>
	<th align="center">Username </th>
	<th align="center">Password</th>
	<th align="center">Nama Petugas</th>
	<th align="center">Nama Level</th>
	<th align="center">Nama Pegawai </th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn, "SELECT * FROM petugas s left join level p on p.id_level=s.id_level
															left join pegawai q on q.id_pegawai=s.id_pegawai");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr>
			<td align="center"><?php echo $no++; ?></td>
			<td><?php echo $data['username']; ?></td>
			<td><?php echo $data['password']; ?></td>
			<td><?php echo $data['nama_petugas']; ?></td>
			<td><?php echo $data['nama_level']; ?></td>
			<td><?php echo $data['nama_pegawai']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Petugas.pdf', 'D');
?>