<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 16%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Data Petugas</h1>
<table border="1" width="100%">
<tr>
								  <th>No.</th>
								  <th>Kode Peminjaman</th>
								  <th>Tanggal Pinjam</th>
								  <th>Status Pinjam</th>
								  <th>ID Pegawai</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn, "SELECT * FROM peminjaman s left join pegawai p on p.id_pegawai=s.id_pegawai where status_pinjam ='Dipinjam'");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr>
			<td><?php echo $no++; ?></td>
				  <td><?php echo $data['kode_pinjam']; ?></td>
				  <td><?php echo $data['tanggal_pinjam']; ?></td>
				  <td><?php echo $data['status_pinjam']; ?></td>
				  <td><?php echo $data['nama_pegawai']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Petugas.pdf', 'D');
?>