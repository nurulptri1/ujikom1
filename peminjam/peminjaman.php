<?php
session_start();
if(!isset($_SESSION['username'])){
	header("Location: ../login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>INVENTARIS user</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Dental Health Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
	
	<!-- css files -->
	<link href="css/css_slider.css" type="text/css" rel="stylesheet" media="all"><!-- slider css -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href="css/style.css" rel='stylesheet' type='text/css' /><!-- custom css -->
    <link href="css/font-awesome.min.css" rel="stylesheet"><!-- fontawesome css -->
	<!-- //css files -->
	
	<!-- google fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
	<!-- //google fonts -->
	
</head>
<body>

<!-- top header -->
<div class="header-top">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				
			</div>
			
		</div>
	</div>
</div>
<!-- //top header -->

<!-- //header -->
<header class="py-3">
	<div class="container">
			<div id="logo">
				<h1> <a href="../login.php"><span class="fa fa-user" aria-hidden="true"></span>&nbsp;INVENTARIS USER</a></h1>
			</div>
		<!-- nav -->
		<nav class="d-lg-flex">

			<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
			<input type="checkbox" id="drop" />
			<ul class="menu mt-2 ml-auto">
				<li class="active"><a href="beranda.php">Beranda</a></li>
				<li class=""><a href="peminjaman.php">Data Pinjam</a></li>
				<li class=""><a href="logout.php">Logout</a></li>
			</ul>
			<div class="login-icon ml-2">
				<a class="user" href="input_pinjam.php"> Pinjam Sekarang</a>
			</div>
		</nav>
		<div class="clear"></div>
		<!-- //nav -->
	</div>
</header>
<!-- //header -->

<!-- banner -->

<!-- //banner -->

<!-- about -->
<section class="about py-5">
	
</section>
<!-- //about -->

<!-- about bottom -->
<section class="mail pt-lg-5 pt-4">
	<div class="container pt-lg-5">
		<h2 class="heading text-center mb-sm-5 mb-4">Data Pinjam</h2>
		<div class="row agileinfo_mail_grids">
			<div class="col-lg-8 agileinfo_mail_grid_right">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>No.</th>
								  <th>Kode Peminjaman</th>
								  <th>Tanggal Pinjam</th>
								  <th>Status Pinjam</th>
								  <th>ID Pegawai</th>
								  <th>Aksi</th>
							  </tr>
						  </thead>   
						  <tbody>
								<?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "ujikom1";
 
            // Membuat Koneksi
            $koneksi = new mysqli($servername, $username, $password, $dbname);
            
            // Melakukan Cek Koneksi
            if ($koneksi->connect_error) {
                die("Koneksi Gagal : " . $koneksi->connect_error);
            } 
 
            //Melakukan query
            $sql = "SELECT * FROM peminjaman s left join pegawai p on p.id_pegawai=s.id_pegawai where status_pinjam='Dipinjam'";
            $hasil = $koneksi->query($sql);
            $no = 1;
            if ($hasil->num_rows > 0) {
                foreach ($hasil as $row) { 
				?>
                  <tr>     
                  <td><?php echo $no; ?></td>
				  <td><?php echo $row['kode_pinjam']; ?></td>
				  <td><?php echo $row['tanggal_pinjam']; ?></td>
				  <td><?php echo $row['status_pinjam']; ?></td>
				  <td><?php echo $row['nama_pegawai']; ?></td>
				 
				  </tr>
            <?php 
            $no++; 
            } 
              } else { 
            echo "0 results"; 
              } $koneksi->close(); 
            ?>
							</tbody>
                                                       </table>
						<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
						<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
						<script>$(document).ready(function(){
							$('#example').DataTable();
						});
						</script>
			</div>
			
		</div>
	</div>
	<div class="map mt-5">
		
	</div>
</section>
<!-- //about bottom -->

<!-- services -->

<!-- //services -->	

<!-- testimonials -->

<!-- //testimonials -->

<!-- blog -->

<!-- //blog -->

<!-- appointment -->

<!-- //appointment -->

<!-- footer -->
<footer class="py-5">
</footer>
<!-- //footer -->

<!-- copyright -->
<div class="copyright">
	<div class="container py-4">
		<div class=" text-center">
			<p>© 2019 INVENTARIS Sarana dan Prasarana SMK.</p>
		</div>
	</div>
</div>
<!-- //copyright -->
		
<!-- move top -->
<div class="move-top text-right">
	<a href="#home" class="move-top"> 
		<span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
	</a>
</div>
<!-- move top -->

</body>
</html>