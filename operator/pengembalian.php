<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Admin</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="../admin/css/bootstrap.min.css" rel="stylesheet">
	<link href="../admin/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="../admin/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="../admin/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
		
		
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>INVENTARIS</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<!-- start: Notifications Dropdown -->
						<!-- end: Notifications Dropdown -->
						<!-- start: Message Dropdown -->
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Account Settings</span>
								</li><li><a href="logout.php"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="beranda.php"><i class="icon-file"></i><span class="hidden-tablet"><b>&nbsp;Beranda</b></span></a></li>
						<li>
							<a class="dropmenu" href="#"><i class="icon-chevron-down"></i><span class="hidden-tablet"><b> Lainnya </b></span></a>
							<ul>
								<li><a class="submenu" href="peminjaman.php"><i class="icon-folder-open"></i><span class="hidden-tablet">&nbsp;Peminjaman</span></a></li>
								<li><a class="submenu" href="pengembalian.php"><i class="icon-tasks"></i><span class="hidden-tablet">&nbsp;Pengembalian</span></a></li>
								<li><a class="submenu" href="laporan_kembali.php"><i class="icon-tasks"></i><span class="hidden-tablet">&nbsp;Laporan Kembali</span></a></li>
							</ul>	
						</li>
						<li><a href="laporan.php"><i class="icon-book"></i><span class="hidden-tablet"><b>&nbsp;Laporan Data</b></span></a></li>
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			
			
			<!-- start: Content -->
			<div id="content" class="span10">
				<div class="module-head">
                                    <h3>
                                        Data Peminjaman</h3>
                                </div>
								<div class="panel-body">
						<div class="control-grup">
						<label>Pilih Id Peminjaman</label>
							<form method="POST">
							<select name="id_peminjaman" class="span8 typeahead">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($conn, "SELECT id_peminjaman,kode_pinjam from peminjaman where status_pinjam='Dipinjam'");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option value='$row[id_peminjaman]'>$row[kode_pinjam]</option>";
								} 
								?>
									</select>
									<br/>
								<button type="submit" name="submit" class="btn btn-outline btn-primary">Data</button>
							</form>
						</div>
					</div>
					<?php
						if(isset($_POST['submit'])){?>
								
							
                               <form action="proses_kembali.php" method="post" role="form">
							            <?php
										include "koneksi.php";
										$id_peminjaman=$_POST['id_peminjaman'];
										$select=mysqli_query($conn, "SELECT * from peminjaman left join detail_pinjam on peminjaman.id_peminjaman=detail_pinjam.id_detail_pinjam
																							  left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
																							  left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris
																							where id_peminjaman='$id_peminjaman'");
																							
										while($data=mysqli_fetch_array($select)){
										?>
										<div class="control-group">
										<input type="hidden" name="id_peminjaman" value="<?php echo $id_peminjaman ?>">
											<label class="control-label" for="disabledInput">ID Peminjaman</label>
											<div class="controls">
												<input name="id_peminjaman" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id_peminjaman'];?>" type="text" placeholder="Disabled input here…" readonly="">
												<input name="id_detail_pinjam" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id_detail_pinjam'];?>" type="hidden" placeholder="Disabled input here…">
											</div>	
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Kode Peminjaman</label>
											<div class="controls">
												<input name="kode_pinjam" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['kode_pinjam'];?>" type="text" placeholder="Disabled input here…" readonly="">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Tanggal Pinjam</label>
											<div class="controls">
												<input name="tanggal_pinjam" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['tanggal_pinjam'];?>" type="text" placeholder="Disabled input here…">
												<input name="tanggal_kembali" type="hidden" class="input-xlarge disabled" id="disabledInput" placeholder="Disabled input here…" disabled="">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Pegawai</label>
											<div class="controls">
												<input name="id_pegawai" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id_pegawai'];?>.<?php echo $data['nama_pegawai'];?>" type="text" placeholder="Disabled input here…">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Nama Barang</label>
											<div class="controls">
												<input name="id_inventaris" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['id_inventaris'];?>" type="text" placeholder="Disabled input here…">
												<input name="nama" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['nama'];?>" type="text" placeholder="Disabled input here…">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="disabledInput">Jumlah</label>
											<div class="controls">
												<input name="jumlah_pinjam" class="input-xlarge disabled" id="disabledInput" value="<?php echo $data['jumlah_pinjam'];?>" type="text" placeholder="Disabled input here…" readonly="">
											</div>
										</div>
										<div class="control-group">
												<label class="control-label" for="basicinput"><b> Status </b></label>
												<select class="span8 typeahead"  name="status_pinjam" required>
													<option value="">Pilih status</option>
                                                        <option>Kembali</option>
												</select>
											</div>
										
										<div class="box-footer">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
                                    </div>
										<?php } ?>
                                 </form>
								<?php } ?>

       

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://themifycloud.com/downloads/janux-free-responsive-admin-dashboard-template/" alt="Bootstrap_Metro_Dashboard">JANUX Responsive Dashboard</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="../admin/js/jquery-1.9.1.min.js"></script>
	<script src="../admin/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="../admin/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="j../admin/s/jquery.ui.touch-punch.js"></script>
	
		<script src="../admin/js/modernizr.js"></script>
	
		<script src="../admin/js/bootstrap.min.js"></script>
	
		<script src="../admin/js/jquery.cookie.js"></script>
	
		<script src='../admin/js/fullcalendar.min.js'></script>
	
		<script src='../admin/js/jquery.dataTables.min.js'></script>

		<script src="j../admin/s/excanvas.js"></script>
	<script src="../admin/js/jquery.flot.js"></script>
	<script src="../admin/js/jquery.flot.pie.js"></script>
	<script src="../admin/js/jquery.flot.stack.js"></script>
	<script src="../admin/js/jquery.flot.resize.min.js"></script>
	
		<script src="../admin/js/jquery.chosen.min.js"></script>
	
		<script src="../admin/js/jquery.uniform.min.js"></script>
		
		<script src="../admin/js/jquery.cleditor.min.js"></script>
	
		<script src="../admin/js/jquery.noty.js"></script>
	
		<script src="../admin/js/jquery.elfinder.min.js"></script>
	
		<script src="../admin/js/jquery.raty.min.js"></script>
	
		<script src="../admin/js/jquery.iphone.toggle.js"></script>
	
		<script src="../admin/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="../admin/js/jquery.gritter.min.js"></script>
	
		<script src="../admin/js/jquery.imagesloaded.js"></script>
	
		<script src="../admin/js/jquery.masonry.min.js"></script>
	
		<script src="../admin/js/jquery.knob.modified.js"></script>
	
		<script src="../admin/js/jquery.sparkline.min.js"></script>
	
		<script src="../admin/js/counter.js"></script>
	
		<script src="../admin/js/retina.js"></script>

		<script src="../admin/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
